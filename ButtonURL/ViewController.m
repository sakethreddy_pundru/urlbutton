//
//  ViewController.m
//  ButtonURL
//
//  Created by admin on 25/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image = [UIImage imageNamed:@"Cosmos07.jpg"];
    UIImageView *imageview = [[UIImageView alloc]initWithImage:image];
    [self.view addSubview:imageview];
    
    UIButton *URL = [[UIButton alloc]init];
    [URL setTitle:@"URL" forState:UIControlStateNormal];
    [URL setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [URL addTarget:self action:@selector(URLClick) forControlEvents:UIControlEventTouchUpInside];
    URL.backgroundColor = [UIColor blueColor];
    URL.frame = CGRectMake(50, 370, 125, 50);
    URL.layer.cornerRadius=30;
    [self.view addSubview:URL];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)URLClick{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
