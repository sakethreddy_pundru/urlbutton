//
//  SecondViewController.m
//  ButtonURL
//
//  Created by admin on 25/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed:@"Cosmos07.jpg"];
    UIImageView *imageview = [[UIImageView alloc]initWithImage:image];
    [self.view addSubview:imageview];
    
    UIButton *URL1 = [[UIButton alloc]init];
    [URL1 setTitle:@"URL" forState:UIControlStateNormal];
    [URL1 setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [URL1 addTarget:self action:@selector(URL) forControlEvents:UIControlEventTouchUpInside];
    URL1.backgroundColor = [UIColor blueColor];
    URL1.frame = CGRectMake(50, 370, 125, 50);
    URL1.layer.cornerRadius=30;
    [self.view addSubview:URL1];
    

    // Do any additional setup after loading the view.
}

-(void)URL{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];
//    [self.navigationController popToViewController:self animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
